var mysql = require('mysql');
var exports = module.exports = {};

/* connect locally
var con = mysql.createConnection({
  host: "localhost",
  user: "natalia.perina",
  password: "sidwell13",
  database: "pmprms2_2017_06_25"
});
*/

var con = mysql.createConnection({
  host: "dddpnhvtenbo2001.cpddxudn0k2k.us-east-1.rds.amazonaws.com",
  user: "dddadmin",
  password: "Cambodia2016",
  database: "pmprms2"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

exports.query = function(sqlquery, callback){
	con.query(sqlquery, function(err, rows){
		//console.log("sqlquery:", sqlquery);
		if (err) throw err; 
		callback(err, rows);
	});

};

exports.submit_comment = function(email, comment, pid, cb) {

  sql = "INSERT INTO productivity_comments VALUES(DEFAULT, " + pid + ", '" + comment + "', '" + email + "', DEFAULT); "
  console.log("SQL:")
  console.log(sql)
  con.query(sql, function(err, rows){
    if (err) throw err; 
    cb(err, rows); 
  });
}


exports.get_comments = function (cb) {


  sql = "SELECT * FROM productivity_comments WHERE created BETWEEN TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY)) AND NOW();";
  con.query(sql, function(err, rows){
    console.log("rows:");
    console.log(rows);
    
    if (err) throw err; 
    cb(err, rows);
  });
};

exports.get_daily_prod = function (start_date, end_date, pid, tid, sid, callback) {


  sql = "SELECT project_id, task_id, subtask_id, DATE_ADD(DATE(start_date), INTERVAL 1 DAY) AS `date`, SUM(TIMESTAMPDIFF(MINUTE, p.start_date, p.end_date))/60 AS `total_duration(hrs)`, SUM(p.units_completed) AS `units_completed`,"
  + " SUM(p.units_completed)/ (SUM(TIMESTAMPDIFF(MINUTE, p.start_date, p.end_date))/60) AS `productivity` "
  + "FROM productivity p WHERE p.start_date BETWEEN STR_TO_DATE('" + start_date +"', '%m/%d/%Y') AND "
  +" STR_TO_DATE('" + end_date + "', '%m/%d/%Y')" + " AND p.project_id = " + pid + " AND p.task_id =" + tid + " AND p.subtask_id = " + sid 
  + " GROUP BY project_id, task_id, subtask_id, DATE_ADD(DATE(start_date), INTERVAL 1 DAY);";
  console.log("sql = "); 
  console.log(sql); 

  tzquery= "SELECT @@global.time_zone, @@session.time_zone;";
  con.query(tzquery, function(err, rows){
    console.log("time zone:");
    console.log(rows);

  });


  con.query(sql, function(err, rows){
    console.log("rows:");
    console.log(rows);
    
    if (err) throw err; 
    callback(err, rows);
  });
};

exports.get_daily_targ = function (pid, tid, sid, callback) {
  sql = "SELECT * FROM target_productivity WHERE project_id = " + pid + " AND task_id = " + tid + " AND subtask_id = " + sid +";"; 


 
  console.log("sql = ", sql); 


  con.query(sql, function(err, rows){
    
    if (err) throw err; 
    callback(err, rows);
  });
};


exports.get_30day_health = function (callback) {
  sql = "CALL 30day_project_health();"; 

  con.query(sql, function(err, rows){
    
    if (err) throw err; 
    callback(err, rows);
  });
};




exports.fiscal_interval_prod = function(start, end, callback){


  sql = "CALL fiscal_interval_productivity(STR_TO_DATE('" + start + "', '%m/%d/%Y'), STR_TO_DATE('" + end +"', '%m/%d/%Y'));";
  con.query(sql, function(err, rows){
    
    if (err) throw err; 
    callback(err, rows);
  });

};

exports.all_interval_prod = function(startdate, enddate, callback){


  sql = "CALL all_interval_productivity(STR_TO_DATE('" + start + "', '%m/%d/%Y'), STR_TO_DATE('" + end +"', '%m/%d/%Y'));";
  con.query(sql, function(err, rows){
    
    if (err) throw err; 
    callback(err, rows);
  });

};




// default home page data
exports.past_30_days_prod = function(callback){
  sql = "CALL 100_day_productivity();"
  con.query(sql, function(err, rows){
    console.log("sqlquery:", sql);
    if (err) throw err; 
    console.log("100 days RESPONSE:");
    console.log(rows); 
    callback(err, rows);
  });
};

// get productivitiy for date interval start - end 
exports.active_interval_prod = function(start, end, callback){
  /* stored procedure sql */ 
  sql = "CALL interval_productivity(STR_TO_DATE('" + start + "', '%m/%d/%Y'), STR_TO_DATE('" + end +"', '%m/%d/%Y'));"
  con.query(sql, function(err, rows){
    console.log("sqlquery:", sql);
    if (err) throw err; 
    callback(err, rows);
  });
};






