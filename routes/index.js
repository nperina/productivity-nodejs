var express = require('express');
var router = express.Router();
var app = express(); 
var db = require('../config/db.js')
var projects = require('../models/projects.js');
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

var send = require('gmail-send')({
	user: 'natalia.perina@digitaldividedata.com'


});

/* GET home page. */
router.get('/productivity/daterange', function(req, res, next){
	console.log("body:", req.body); 
	console.log("req.params", req.params); 
	console.log("req.query", req.query ); 
	start_date = req.query.date1; 
	end_date = req.query.date2; 
	res.redirect('/'); 


}); 

router.get('/bug_reporter', function (req, res, next){
	res.render('bug_report'); 

}); 

router.post("/api/sessions/:off", function(req, res, next){
	var sessions = req.body.sessions; 
	var officeId; 
	if (req.params.off == "pnh"){
		officeId = '1';
	}
	else if (req.params.off == "vte"){
		officeId = '2';
	}
	else if (req.params.off == "nbo"){
		officeId = '3';
	}
	else {
		res.json({erorr: "Cannot parse office parameter!"});
		return; 
	}



	var sql = "INSERT INTO associate_comp_sessions (user_id, office_id, logon, logoff, duration, lockouts, lockoutsTotalDuration) VALUES ";
	var i = 0; 
	var noDatCount = 0;
	sessions.forEach(function(session){
		if (!(session.user) || !(session.logOnTime) || !(session.logOffTime) || !(session.duration) || !(session.lockOutCount)){
			console.log("data not defined.");
			noDataCount++;
			return; 
		}
		sql = sql + "('" + session.user + "', " + officeId + ", ";
		sql = sql + "STR_TO_DATE('" + session.logOnTime.split(".")[0] +"', '%Y-%m-%dT%H:%i:%s'), "
		sql = sql + "STR_TO_DATE('" + session.logOffTime.split(".")[0] +"', '%Y-%m-%dT%H:%i:%s'), "
		sql = sql + parseInt(session.duration/1000) + ", "
		sql = sql + session.lockOutCount + ", "
		if (i == sessions.length - 1){
			sql = sql + parseInt(session.totalLockOutDuration/1000) + ")"
		}
		else {
			sql = sql + parseInt(session.totalLockOutDuration/1000) + "), "
		}
		i++; 
	});
	sql = sql + " ON DUPLICATE KEY UPDATE;"
	console.log(sql);

	if (noDataCount == sessions.length){
		res.json({"error": "Every session was missing vital data."})
	}

	db.query(sql, function(error, results){
		if (error){
			res.json({"error": error});
		}
		else {
			res.json({"result": results}); 
		}
		
	});

});


router.get("/api/sessions/:off", function(req, res, next){
	if (req.params.off == "pnh"){
		officeId = '1';
	}
	else if (req.params.off == "vte"){
		officeId = '2';
	}
	else if (req.params.off == "nbo"){
		officeId = '3';
	}
	else {
		res.json({erorr: "Cannot parse office parameter!"});
		return; 
	}
	var sql = "SELECT * FROM this_month_user_logins WHERE office_id = " + officeId;
	db.query(sql, function(error, results){
		if (error){
			res.json({"error": error});
		}
		else {
			res.json({"result": results}); 
		}
	});
});



router.get('/api/dailyprod/:off', function(req, res, next){
	console.log("request.params: ");
	console.log(req.params); 
	var sql; 
	if (req.params.off == 'pnh'){
		sql = "SELECT * FROM hours_per_day_this_month WHERE office = 'PNH';"
	}
	else if (req.params.off == 'vte'){
		sql = "SELECT * FROM hours_per_day_this_month WHERE office = 'VTE';"
	}
	else if (req.params.off == 'nbo'){
		sql = "SELECT * FROM hours_per_day_this_month WHERE office = 'NBO';"
	}
	else if (req.params.off == 'all'){
		sql = "SELECT * FROM hours_per_day_this_month;"
	}
	else {
		res.json({error: "Unable to parse that office location."})
	}
	db.query(sql, function(error, results){
		res.json({hourlyData: results}); 
	});

})

router.get('/api/employees/:off', function(req, res, next){
	var offid; 
	if (req.params.off == 'pnh'){
		offid = "1";
	}
	else if (req.params.off == 'vte'){
		offid = "2";
	}
	else if (req.params.off == 'nbo'){
		offid = "3";
	}
	else if (req.params.off == 'all'){
		var sql = "SELECT DISTINCT prod.employee_id, emp.employee_id AS `empID`, CONCAT(emp.given_name, ' ', emp.family_name) AS `empName`, "
		+ " CONCAT(supEmp.given_name, ' ', supEmp.family_name) as `supervisor` FROM productivity prod "
		+ " LEFT JOIN employee emp ON prod.employee_id = emp.id "
		+ " LEFT JOIN details ON emp.id = details.employee_id "
		+ " LEFT JOIN employee supEmp ON details.supervisor_id = supEmp.id "
		+ " WHERE DATE(prod.start_date) BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND CURDATE() " 
		+ " AND details.employee_category = 'direct operation' AND emp.employee_id IS NOT NULL ORDER BY empName"
		db.query(sql, function(error, results){
			res.json({associates: results}); 
			return; 
		});
	}
	else {
		res.json({error: "Unable to parse that office location."});
		return; 
	}

	var sql = "SELECT DISTINCT prod.employee_id, emp.employee_id AS `empID`, CONCAT(emp.given_name, ' ', emp.family_name) AS `empName`, "
		+ " CONCAT(supEmp.given_name, ' ', supEmp.family_name) as `supervisor` FROM productivity prod "
		+ " LEFT JOIN employee emp ON prod.employee_id = emp.id "
		+ " LEFT JOIN details ON emp.id = details.employee_id "
		+ " LEFT JOIN employee supEmp ON details.supervisor_id = supEmp.id "
		+ " WHERE DATE(prod.start_date) BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND CURDATE() AND details.office_id = " + offid
		+ " AND details.employee_category = 'direct operation' AND emp.employee_id IS NOT NULL ORDER BY empName"
		db.query(sql, function(error, results){
			res.json({associates: results}); 
		});

	

});


router.get('/week_low', function (req, res, next){
	console.log("in weekly report!")
	db.get_30day_health(function(err, rows){
		res.render('weekly_report', {projects: rows[0], high: false}); 
	}); 
});

router.get('/week_high', function (req, res, next){
	console.log("in weekly report!")
	db.get_30day_health(function(err, rows){
		res.render('weekly_report', {projects: rows[0], high: true}); 
	}); 
});

router.post('/submit_bug', function (req, res, next){
	

	res.redirect('/'); 

}); 


router.post('/subtask', function(req, res, next){
	console.log(req.body);
	pid = req.body.pid;
	tid = req.body.tid;
	sid = req.body.sid;
	start_date = req.body.sdate;
	end_date = req.body.edate;
	console.log("start_date: " + start_date);
	console.log("end_date " + end_date); 
	console.log("pid : ");
	console.log(pid); 
	console.log("sid: ");
	console.log(sid);
	console.log("tid: ");
	console.log(tid);
	db.get_daily_prod(start_date, end_date, pid, tid, sid, function (err, prods) {
		//console.log("prods:"); 
		//console.log(prods); 
		db.get_daily_targ(pid, tid, sid, function(err, targets){
			//console.log("targets: ");
			//console.log(targets);

			projects.ActiveCount().then(function(count) {
  		//console.log("Success! Active count: ", count);
  				active_count = count; 
				projects.FSCount().then(function(fscount){
				res.render('subtask', {fiscal_count: fscount, active_count:active_count, prods: prods, 
					targets: targets, start_date: start_date, end_date: end_date}); 
			}).catch(function(e){
				console.log(e); 
			});


		}).catch(function(e){
			console.log(e)
		})
		
		}); 
	});
}); 

// submits a comment into the productivity table. 
router.post('/submit_comment', function(req, res, next){
	pid = req.body.pid 
	email = req.body.email 
	comment = req.body.comment
	console.log("pid:")
	console.log(pid)
	console.log("email")
	console.log(email)
	console.log("comment")
	console.log(comment)
	db.submit_comment(email, comment, pid, function(err, rows){
		res.render('submit_comment')
	}); 
}); 


router.post('/graph', function(req, res, next){
	//console.log("body:");
	//console.log(req.body);
	pid = req.body.pid;
	tid = req.body.tid;
	sid = req.body.sid;
	start_date = req.body.sdate;
	end_date = req.body.edate;
	console.log("start_date: " + start_date);
	console.log("end_date " + end_date); 
	console.log("pid : ");
	console.log(pid); 
	console.log("sid: ");
	console.log(sid);
	console.log("tid: ");
	console.log(tid);


	db.get_daily_prod(start_date, end_date, pid, tid, sid, function (err, prods) {
		//console.log("prods:"); 
		//console.log(prods); 
		db.get_daily_targ(pid, tid, sid, function(err, targets){
			console.log("targets: ");
  		//console.log("Success! Active count: ", count);
				res.send({prods: prods, 
					targets: targets, start_date: start_date, end_date: end_date});
		
		}); 
	});
});




function render_data(productivity, projects){
	var returner_projects = []; 
	projects.forEach( function(project){
				//console.log("active project id :", project.project_id);
				id = project.project_id;
				//console.log("active project name :", project.project_name);
				subtasks = productivity.filter(x => x.project_id === id);
				//console.log('subtasks:', subtasks); 

				task_ids = [];
				tasks  = []; 
				for ( i = 0; i < subtasks.length; i++){
					if (task_ids.indexOf(subtasks[i].task_id) == - 1) {
						task_ids.push(subtasks[i].task_id);
						task = {};
						task['task_id'] = subtasks[i].task_id; 
						task['task_name'] = subtasks[i].task_name; 
						task['subtasks'] = subtasks.filter(x => x.task_id === subtasks[i].task_id);
						tasks.push(task); 
						//console.log("adding task "); 
					}
				}
				total_count = 0; 
				total_count_met = 0; 
				tasks.forEach(function(task){
					task_count = 0; 
					task_count_met = 0; 
					task.subtasks.forEach(function(st){
						/*
						if (st.total_hour_forecast != null && st.total_hour_forecast != 0 && 
							st.total_unit_forecast != null && st.total_unit_forecast != 0 && 
							task.task_name != "Project Downtime" && task.task_name != "Project Management" && st['total_duration(hrs)'] != 0 )
						*/
						if (st.productivity != null &&
							st.target_productivity != null && 
							task.task_name != "Project Downtime" && task.task_name != "Project Management" && 
							st.productivity != 0)
						{
							if (st.productivity > st.target_productivity){
								task_count_met++; 
								total_count_met++;
							}
							task_count++; 
							total_count++; 
						}
					});
					task['num_subtasks_met'] = task_count_met; 
					task['num_subtasks'] = task_count; 
					task['score'] = ((task_count_met / task_count) * 100).toFixed(2); 
					if (task_count_met == 0 && task_count == 0) {
						task['score'] = null; 
					}
					if (task.subtasks == null || task.subtasks.length == 0 || 
						task_count == 0 ){
						task['subtasks'] = null; 
						task['num_subtasks_met'] = null; 
						task['num_subtasks'] = null; 
						task['score'] = null;
					}

				});
				project['num_subtasks_met'] = total_count_met; 
				project['num_subtasks'] = total_count; 
				
				if (total_count == 0 ) {
					project['score'] = null;
				}
				else {
					project['score'] = (total_count_met / total_count) * 100; 
				} 
				project['tasks'] = tasks;
				if (project['score'] != null){
					returner_projects.push(project);

				} 
			});
			//console.log("ACTIVE PROJECTS: "); 
			//console.log(active_projects); 
			//
			//var page_data = {projects:projects};
			var page_data = {projects:returner_projects};
			return page_data; 
}

router.get('/', function(req , res, next){
	res.render('index_login');


});

router.get('/dashboard', function(req, res, next) {
	var fiscal_count; 
	var active_count; 
	var start_date; 
	var end_date; 

	// custome date range? True if user selected a date range on the dashboard (always true except first page landing)
	var date_range = false; 
	// show currently active projects? Default home page is Active projects 
	var active = true; 
	// show fiscal year projects? True if user selected fiscal year projects from the drop down menu 
	var fiscal = false; 
	// show all projects? True if user selected all projects from the drop down menu 
	var all = false; 

	//set options as selected from the drop down menus
	if (req.query.date1 != null && req.query.date2 != null && 
		req.query.sel1 != null){
		start_date = req.query.date1; 
		end_date = req.query.date2;
		type = req.query.sel1;
		date_range = true; 
		if (type == "all") {
			all = true; 
			active = false; 
		}
		else if (type == "fiscal"){
			fiscal = true; 
			active = false; 
		}
		console.log("select:", type); 
		console.log('start date:', start_date);
		console.log('end date: ', end_date); 
	}

	// get number of currently active projects 
	projects.ActiveCount().then(function(count) {
  		//console.log("Success! Active count: ", count);
  		active_count = count; 

  		// get number of projects this fiscal year
		projects.FSCount().then(function(fscount){
		fiscal_count = fscount; 
		if (date_range) 
		{
				// get projects for fiscal year 
				if (fiscal){
					db.fiscal_interval_prod(start_date, end_date, function(err, productivity){
						db.get_comments(function(err, comments){
							projects.getFiscal().then(function(fiscal_projects){
								page_data = render_data(productivity[0], fiscal_projects); 
								page_data['start_date'] = start_date; 
								page_data['end_date'] = end_date; 
								page_data['active_count'] = active_count;
								page_data['fiscal_count'] = fiscal_count; 
								page_data['fiscal_selected'] = fiscal; 
								page_data['all_selected'] = all; 
								page_data['comments'] = comments;
								res.render('index_table', page_data);
							}).catch(function(e){
								console.log(e); 
							});
						});
					})
					
				}
				else if (active){
					db.active_interval_prod(start_date, end_date, function(err, productivity){
						db.get_comments(function(err, comments){
							projects.getActive().then(function(active_projects){
								page_data = render_data(productivity[0], active_projects); 
								page_data['start_date'] = start_date; 
								page_data['end_date'] = end_date; 
								page_data['active_count'] = active_count;
								page_data['fiscal_count'] = fiscal_count; 
								page_data['fiscal_selected'] = fiscal; 
								page_data['all_selected'] = all; 
								page_data['comments'] = comments;
								res.render('index_table', page_data);
							}).catch(function(e){
								console.log(e); 
							});
						});
					});
				}

				else if (all){
					db.all_interval_prod(start_date, end_date, function(err, productivity){
						db.get_comments(function(err, comments){
							projects.getAll().then(function(active_projects){
								page_data = render_data(productivity[0], active_projects); 
								page_data['comments'] = comments;
								page_data['start_date'] = start_date; 
								page_data['end_date'] = end_date; 
								page_data['active_count'] = active_count;
								page_data['fiscal_count'] = fiscal_count; 
								page_data['fiscal_selected'] = fiscal; 
								page_data['all_selected'] = all; 
								res.render('index_table', page_data);
							}).catch(function(e){
								console.log(e); 
							});
						});
					});
				}
			
		}

		//default home dashboard entrance: 
		else {
			db.past_30_days_prod(function(err, productivity){
			productivity = productivity[0]; 
			db.get_comments(function(err, comments){
				//default home page
				if (active){
						projects.getActive().then(function(active_projects){
							page_data = render_data(productivity, active_projects); 
							page_data['comments'] = comments; 
							page_data['start_date'] = start_date; 
							page_data['end_date'] = end_date; 
							page_data['active_count'] = active_count;
							page_data['fiscal_count'] = fiscal_count; 
							page_data['fiscal_selected'] = fiscal;
							page_data['all_selected'] = all;
							res.render('index_table', page_data);

						}).catch(function(e)
						{
								console.log(e);
						}); 
				}

				// not ever used? i dont think... 
				else if (fiscal){
						projects.getFiscal().then(function(fiscal_projects){
							page_data = render_data(productivity[0], fiscal_projects); 
							page_data['start_date'] = start_date; 
							page_data['end_date'] = end_date; 
							page_data['active_count'] = active_count;
							page_data['fiscal_count'] = fiscal_count; 
							page_data['fiscal_selected'] = fiscal; 
							page_data['all_selected'] = all;
							res.render('index_table', page_data);
						}).catch(function(e){
							console.log(e); 
						});
					}



			})

			

			});
		}


	}).catch(function(e){
		console.log(e); 
	});

	});
});


module.exports = router;
