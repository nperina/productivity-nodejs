var express = require('express');
var router = express.Router();
var db = require('../config/db.js')

var exports = module.exports = {};

/* return the number of projects in fiscal year*/ 
exports.projFSCount(function(cb){
	sql = "SELECT COUNT(*) FROM projects WHERE start_date BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'),"  
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW()"
	+ " OR `end_date` BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW();"
	db.query(sql, function(results, fields){
		cb(results[0]); 
	});
});


exports.projActiveCount(function(cb){
	sql = "SELECT COUNT(*) FROM projects WHERE NOW() BETWEEN start_date AND end_date;"
	db.query(sql, function(results, fields){
		cb(results[0]); 
	});
});




/* return the number of current
exports.projActiveCount(function(cb){
	sql = "SELECT COUNT(*) FROM projects WHERE start_date BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'),"  
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW()"
	+ " OR `end_date` BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW();"
	db.query(sql, function(results, fields){
		cb(results[0]); 
	});
});*/