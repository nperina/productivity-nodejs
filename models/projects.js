var express = require('express');
var router = express.Router();
var db = require('../config/db.js')

var exports = module.exports = {};

/* return the number of projects in fiscal year*/ 
exports.FSCount = function(){
	sql = "SELECT COUNT(*) FROM projects pr WHERE pr.end_date >= IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 7), "
    + " STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
    + "STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW())), '%M %d,%Y'));";
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){
			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows[0]['COUNT(*)']);
		});
	}); 
}

exports.getFiscal = function() {
	/* old code 
	sql = "SELECT pr.name as `project_name`, pr.id as `project_id`, off.office_name as `office`, DATE_FORMAT(pr.start_date, '%Y-%m-%d') AS start_date,"
	+"DATE_FORMAT(pr.end_date, '%Y-%m-%d') AS end_date FROM projects pr LEFT JOIN lu_office off ON off.id = pr.projects_location "
	+ "WHERE pr.start_date BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 7), "
	+ "STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW()) - 1), '%M %d,%Y'),"  
	+ "STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW()"
	+ " OR pr.`end_date` BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 7), STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
	+ "STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW();"
	*/ 

	sql = "SELECT pr.name AS `project_name`, pr.id AS `project_id`, off.office_name AS `office`, DATE_FORMAT(pr.start_date, '%Y-%m-%d') AS start_date,"
    + " DATE_FORMAT(pr.end_date, '%Y-%m-%d') AS end_date FROM projects pr LEFT JOIN lu_office off ON off.id = pr.projects_location "
    + " WHERE pr.end_date >= IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 7), "
    + " STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
    + "STR_TO_DATE(CONCAT('July 1, ', YEAR(NOW())), '%M %d,%Y'));";

	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){
			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows);
		});
	}); 



}


exports.ActiveCount = function(){
	sql = "SELECT COUNT(*) FROM projects proj LEFT JOIN projects_status ps ON ps.id = proj.projects_status_id WHERE ps.status_group = 'open';"
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){

			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows[0]['COUNT(*)']);
		});
	}); 
}

exports.getAll = function(){
	sql = "SELECT projects.`name` AS `project_name`, projects.id AS `project_id`, off.office_name as `office`, DATE_FORMAT(projects.start_date, '%Y-%m-%d') AS start_date,"
	+"DATE_FORMAT(projects.end_date, '%Y-%m-%d') AS end_date FROM projects LEFT JOIN lu_office off ON off.id = projects.projects_location WHERE in_trash IS NULL; "
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){

			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows);
		});
	}); 
}


exports.getActive = function(){
	sql = "SELECT projects.`name` AS `project_name`, projects.id AS `project_id`, off.office_name AS `office`, DATE_FORMAT(projects.start_date, '%Y-%m-%d') AS start_date,"
		+ " DATE_FORMAT(projects.end_date, '%Y-%m-%d') AS end_date FROM projects LEFT JOIN lu_office off ON off.id = projects.projects_location LEFT JOIN projects_status ps" 
		+ " ON ps.id = projects.projects_status_id WHERE ps.status_group = 'open'; "
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){

			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows);
		});
	}); 
}

exports.getTasks = function(project_id, cb){
	sql = "SELECT tp.task_id, tp.project_id, tasks.name as `task_name` FROM tasks_projects tp "
	+ "LEFT JOIN tasks ON tasks.id = tp.task_id WHERE tp.project_id = " + project_id + " ; "
	db.query(sql, function(err, rows){

			if (err) console.log(err);
			else cb(err, rows);
	});
};


exports.getTaskSubtaskProd = function(project_id, task_id, cb){

	sql = "SELECT P.project_id AS `project_id`, "
	+ "st.subtask_name AS `subtask_name`, P.`total_duration(hrs)`, P.units_completed, P.`productivity`, R.total_hour_forecast, "
	+ " R.total_unit_forecast, R.target_productivity FROM ("
	+ "(SELECT project_id, task_id, subtask_id, SUM(TIMESTAMPDIFF(HOUR, p.start_date, p.end_date)) AS `total_duration(hrs)`, SUM(p.units_completed) AS `units_completed`, "
	+ " SUM(p.units_completed)/ SUM(TIMESTAMPDIFF(HOUR, p.start_date, p.end_date)) AS `productivity` "
	+ "FROM productivity p WHERE p.start_date BETWEEN DATE_SUB(NOW(), INTERVAL 100 DAY) AND NOW() AND p.end_date BETWEEN DATE_SUB(NOW(), INTERVAL 100 DAY) AND NOW() "
	+ "AND p.project_id = " + project_id + " AND p.task_id = " + task_id
	+ " GROUP BY project_id, task_id, subtask_id) AS P LEFT JOIN "
	+ "(SELECT project_id, task_id, subtask_id, SUM(volume_unit_forecast) AS `total_unit_forecast`, "
	+ "SUM(resource_hour_forecast) AS `total_hour_forecast`, SUM(volume_unit_forecast) / SUM(resource_hour_forecast) AS `target_productivity` "
	+ "FROM subtasks_projects GROUP BY project_id, task_id, subtask_id  ) AS R "
	+ "ON P.project_id = R.project_id AND P.task_id = R.task_id AND P.subtask_id = R.subtask_id "
	+ "LEFT JOIN subtasks st ON st.subtask_id = P.subtask_id ); "
	db.query(sql, function(err, rows){

			if (err) console.log(err);
			else cb(err, rows);
	});
};

exports.getActiveProductivity = function(){
	sql = "SELECT * FROM project_productivity WHERE NOW() BETWEEN `project start` AND `project end`;"
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){
			console.log("response rows:", rows)

			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows);
		});
	}); 
}


exports.getAllProductivity = function(){
	sql = "SELECT * FROM project_productivity;"
	return new Promise (function(resolve, reject){
		db.query(sql, function(err, rows){
			console.log("response rows:", rows)

			if (err) reject(Error("SQL error: could get ActiveCount."));
			else resolve(rows);
		});
	}); 
}




/* return the number of current
exports.projActiveCount(function(cb){
	sql = "SELECT COUNT(*) FROM projects WHERE start_date BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'),"  
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW()"
	+ " OR `end_date` BETWEEN IF ((MONTH(NOW()) >= 1 AND MONTH(NOW()) < 10), STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW()) - 1), '%M %d,%Y'), "
	+ "STR_TO_DATE(CONCAT('October 1, ', YEAR(NOW())), '%M %d,%Y')) AND NOW();"
	db.query(sql, function(results, fields){
		cb(results[0]); 
	});
});*/ 